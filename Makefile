# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pkerckho <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/11 11:40:18 by pkerckho          #+#    #+#              #
#    Updated: 2016/03/28 16:34:38 by pkerckho         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FLAGS = -Wall -Wextra -Werror

NAME = wolf3d

MLX = -L minilibx_macos -lmlx -framework Opengl -framework AppKit

SRC = main.c \
	  parse.c \
	  print.c \
	  calculs.c \
	  move.c \
	  key_biding.c \
	  utilities.c

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	@make -C libft
	@make -C minilibx_macos
	@gcc $(FLAG) -o $(NAME) $(OBJ) $(MLX) -L libft -lft
	@echo "$(NAME) created"

%.o: %.c
		@gcc $(FLAG) -c $< -o $@
clean:
	@make clean -C libft
	@make clean -C minilibx_macos
	@rm -f $(OBJ)
	@echo "$(NAME) OBJ deleted"

fclean: clean
	@make fclean -C libft
	@rm -f $(NAME)
	@echo "$(NAME) deleted"

re: fclean all

.PHONY: all, clean, fclean, re
