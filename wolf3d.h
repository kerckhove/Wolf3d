/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pkerckho <pkerckho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 10:27:54 by pkerckho          #+#    #+#             */
/*   Updated: 2016/03/29 10:36:47 by pkerckho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# define WIN_X 1280
# define WIN_Y 800

# define MAP1   "maps/lvl0"
# define MAP2   "maps/lvl1"
# define MAP3   "maps/lvl2"
# define SKY "textures/sky.xpm"

# define KEYPRESS 2
# define KEYRELEASE 3
# define KEYPRESSMASK (1L<<0)
# define KEYRELEASEMASK (1L<<1)

# define LEFT 0
# define RIGHT 2
# define UP 13
# define DOWN 1
# define PLUS 69
# define MINUS 78
# define STAR 67
# define SLASH 75
# define PAGE_UP 116
# define PAGE_DOWN 121
# define CLEAR 71
# define ESC 53
# define ONE 18
# define TWO 19
# define THREE 20

# define COLOR_SKY 0xffEAFF
# define COLOR_FLOOR 0x00ff96
# define COLOR_NORTH 0Xff7d0a
# define COLOR_SOUTH 0xc41f3b
# define COLOR_EAST 0x9482c9
# define COLOR_WEST 0X0070de

# define WELCOME   "Wolf3d by pkerckho"
# define SELECT    "  Select a level  "
# define LEVEL_KEY " LEVEL  |  KEY    "
# define LEVEL1    "   1    |   1     "
# define LEVEL2    "   2    |   2     "
# define LEVEL3    "   3    |   3     "

# define CONTROLS "     CONTROLS "
# define H_MOVE "     shifting    |    W,S"
# define H_ROTATE "     shifting    |    A,D"
# define H_ZERO "     reset       |    clear"
# define H_ESC "     exit        |    esc"

# include "minilibx_macos/mlx.h"
# include <fcntl.h>
# include <unistd.h>
# include <math.h>
# include "libft/includes/libft.h"

typedef struct				s_xy
{
	int						x;
	int						y;
}							t_xy;

typedef struct				s_dxy
{
	double					x;
	double					y;
}							t_dxy;

typedef struct				s_img
{
	void					*im;
	char					*imc;
	int						imlen;
	int						bpp;
	int						endi;
}							t_img;

typedef struct				s_env
{
	char					*choice;
	int						fd;
	char					**line;
	int						cnt_line;
	int						cnt_col;
	int						**map;
	int						check;

	void					*mlx;
	void					*win;
	int						color;

	t_img					img;
	t_img					sky;

	double					camera;
	int						wall;
	int						hit;

	t_dxy					pos;
	t_dxy					dir;
	t_dxy					rplane;
	t_dxy					rpos;
	t_dxy					rdir;
	t_dxy					rdist;
	t_dxy					rdisd;
	t_xy					rmap;
	t_xy					step;
	int						wstart;
	int						wend;
	int						rh;

	double					speed;
	int						left;
	int						right;
	int						up;
	int						down;

}							t_env;

void						ft_parse(t_env *e, char *line);
void						ft_put_help(t_env e);
void						ft_put_pixel(t_env *e, int x, int y, int color);
int							ft_key_hit(int keycode, t_env *e);
int							ft_key_release(int keycode, t_env *e);
int							ft_core(t_env *e);
void						ft_disp_screen(t_env *e);
void						ft_size_ray(t_env *e);
void						ft_hit_ray(t_env *e);
void						ft_direction_ray(t_env *e);
void						ft_init_ray(t_env *e, int x);
void						ft_move(t_env *e);
void						ft_settings(t_env e);
void						ft_welcome(t_env e);
void						ft_init(t_env *e);

#endif
