/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utilities.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pkerckho <pkerckho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 13:39:34 by pkerckho          #+#    #+#             */
/*   Updated: 2016/03/29 10:42:14 by pkerckho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_welcome(t_env e)
{
	int		m_x;
	int		m_y;

	m_x = 510;
	m_y = 200;
	mlx_string_put(e.mlx, e.win, m_x, m_y, 0x007fff, WELCOME);
	mlx_string_put(e.mlx, e.win, m_x, m_y + 20, 0X007fff, SELECT);
	mlx_string_put(e.mlx, e.win, m_x, m_y + 50, 0X007fff, LEVEL_KEY);
	mlx_string_put(e.mlx, e.win, m_x, m_y + 70, 0Xfff000, LEVEL1);
	mlx_string_put(e.mlx, e.win, m_x, m_y + 90, 0Xff2052, LEVEL2);
	mlx_string_put(e.mlx, e.win, m_x, m_y + 110, 0x138808, LEVEL3);
}

void	ft_settings(t_env e)
{
	int		m_x;
	int		m_y;

	m_x = 1000;
	m_y = 0;
	mlx_string_put(e.mlx, e.win, m_x, m_y, 0Xffffff, CONTROLS);
	mlx_string_put(e.mlx, e.win, m_x, m_y + 16, 0Xffffff, H_MOVE);
	mlx_string_put(e.mlx, e.win, m_x, m_y + 32, 0Xffffff, H_ROTATE);
	mlx_string_put(e.mlx, e.win, m_x, m_y + 48, 0Xffffff, H_ZERO);
	mlx_string_put(e.mlx, e.win, m_x, m_y + 64, 0Xffffff, H_ESC);
}
