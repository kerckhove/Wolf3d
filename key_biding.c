/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_biding.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pkerckho <pkerckho@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 11:13:17 by pkerckho          #+#    #+#             */
/*   Updated: 2016/03/28 16:31:26 by pkerckho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		ft_key_hit(int keycode, t_env *e)
{
	if (!e->check && (keycode == ONE || keycode == TWO))
		keycode == ONE ? ft_parse(e, MAP1) : ft_parse(e, MAP2);
	if (!e->check && keycode == THREE)
		ft_parse(e, MAP3);
	if (keycode == UP)
		e->up = 1;
	if (keycode == DOWN)
		e->down = 1;
	if (keycode == LEFT)
		e->left = 1;
	if (keycode == RIGHT)
		e->right = 1;
	if (keycode == CLEAR)
		ft_init(e);
	if (keycode == ESC)
		exit(1);
	return (0);
}

int		ft_key_release(int keycode, t_env *e)
{
	if (keycode == UP)
		e->up = 0;
	if (keycode == DOWN)
		e->down = 0;
	if (keycode == LEFT)
		e->left = 0;
	if (keycode == RIGHT)
		e->right = 0;
	if (keycode == ESC)
		exit(1);
	return (0);
}

int		ft_core(t_env *e)
{
	if (e->check == 0)
		return (0);
	mlx_destroy_image(e->mlx, e->img.im);
	e->img.im = mlx_new_image(e->mlx, WIN_X, WIN_Y);
	ft_move(e);
	ft_disp_screen(e);
	mlx_put_image_to_window(e->mlx, e->win, e->sky.im, 0, 0);
	mlx_put_image_to_window(e->mlx, e->win, e->img.im, 0, 0);
	ft_settings(*e);
	return (0);
}
